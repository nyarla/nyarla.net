.PHONY: all serve deploy

all:
	@echo "Usage: make [serve|deploy]"

serve:
	@hugo serve --bind 192.168.137.254 -p 8080

deploy:
	rm -rf public/*
	hugo
	aws s3 sync public/ s3://nyarla.net/	
